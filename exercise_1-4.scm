(define (a-plus-abs-b a b)
	((if (> b 0) + -) a b))

(a-plus-abs-b 3 -3) 
; (- 3 -3)
; 6

(a-plus-abs-b 3 1) 
; (+ 3 1)
; 4
