(define (p) (p))

(define (test x y)
	(if (= x 0)
			0
			y))

(test 0 (p))

; An interpreter that uses applicative-order evaluation will get 
; stuck in an endless loop because (p) is evaluated when calling test
; 
; An interpreter that uses normal-order evaluation will print 0
