10 ; 10

(+ 5 3 4) ; 12

(- 9 1) ; 8

(/ 6 2) ; 3

(+ (* 2 4) (- 4 6)) ; 6

(define a 3) ; 2

(define b (+ a 1)) ; 3

(+ a b (* a b)) ; 11

(= a b) ; #f

(if (and (> b a) (< b (* a b)))
		b
		a) ; 3

(cond ((= a 4) 6)
			((= b 4) (+ 6 7 a))
			(else 25)) ; 25

(+ 2 (if (> b a) b a)) ; 5

(* (cond ((> a b) a)
				 ((< a b) b)
				 (else -1))
	 (+ a 1)) ; 9
