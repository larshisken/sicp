(define (square a) (* a a))

(define (sum-of-squares-two-larger a b c)
	(- (+ 
			 square(a)
			 square(b)
			 square(c))
		 (square 
			 (min a b c))))
