; Results for very small numbers may be inaccurate because of the 
; relatively large tolerance. Results for very large numbers may
; take forever to calculate.
;
; The alternative approach to the good-enough? procedure should yield
; better results for small numbers because it uses a relative value
; to determine it's result.

(define (square x) (* x x))

(define (average x y)
	(/ (+ x y) 2))

(define (improve guess x)
	(average guess (/ x guess)))

; (define (good-enough? guess x)
; 	(< (abs (- (square guess) x)) 0.001))

; The relative good-enough implementation
(define (good-enough? guess x)
	(< (abs (- (improve guess x) guess))
		 (abs (* guess 0.001))))

(define (sqrt-iter guess x)
	(if (good-enough? guess x)
			guess
			(sqrt-iter (improve guess x)
								 x)))

(define (sqrt x)
	(sqrt-iter 1.0 x))
